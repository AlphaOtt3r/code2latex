import argparse
import os

langs = []

def read_file(path:str) -> str:
    with open(path, "r") as file:
        text = "\r".join(file.readlines())
    
    return text

def replace_tex_placeholder(placeholder:str, replace:str, text:str) -> str:
    text = text.replace(placeholder, replace)
    return text

def argparser() -> object:
    ap = argparse.ArgumentParser("Convert your code to latex")
    ap.add_argument("-c", "--code", type=str, required=True, 
            help="The path of the code file.")
    ap.add_argument("-t", "--text", type=str, required=False, 
            help="The path of the textfile, whcih will be added below the code.")
    ap.add_argument("-o", "--output", type=str, required=True, 
            help="The name of the final pdf file")

    return ap.parse_args()

def main() -> None:
    ap      = argparser()
    lat_tmp = read_file("./latex_template.txt")
    code    = read_file(ap.code)
    name    = f"{ap.output}.tex"

    tex_text = replace_tex_placeholder("%--code--%", code, lat_tmp)
    
    if ap.text == None: 
        tex_text = replace_tex_placeholder("%--text--%", "", tex_text)
    else: 
        tex_text = replace_tex_placeholder("%--text--%", read_file(ap.text), tex_text)
    
    with open(name, "w") as file:
        file.write(tex_text)

    os.system(f"pdftex --shell-escape {name}")
    #os.system(f"rm {name}")

if __name__ == "__main__":
    main()
